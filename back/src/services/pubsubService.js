const { PubSub } = require("@google-cloud/pubsub");

const pubsub = new PubSub({
  projectId: "leprojetsubv",
  keyFilename: "leprojetsubv-03afe917999e.json",
});

module.exports = pubsub;
