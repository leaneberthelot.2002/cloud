const pubsubService = require("../services/pubsubService");

// Logique pour la gestion des topics et subscriptions
exports.createTopic = async (req, res) => {
  // Implémentation de la création d'un topic dans Pub/Sub
};

exports.deleteTopic = async (req, res) => {
  // Implémentation de la suppression d'un topic dans Pub/Sub
};
