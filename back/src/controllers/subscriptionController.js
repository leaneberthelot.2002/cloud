const pubsubService = require("../services/pubsubService");


exports.createSubscription = async (req, res) => {
  // Implémentation de la création d'une subscription dans Pub/Sub
};

exports.deleteSubscription = async (req, res) => {
  // Implémentation de la suppression d'une subscription dans Pub/Sub
};
