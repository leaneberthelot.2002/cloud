const express = require("express");
const router = express.Router();
const subscriptionController = require("../controllers/subscriptionController");

router.post("/create", subscriptionController.createSubscription);
router.delete(
  "/delete/:topicName/:subscriptionName",
  subscriptionController.deleteSubscription
);

module.exports = router;
