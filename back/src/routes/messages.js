const express = require("express");
const router = express.Router();
const messageController = require("../controllers/messageController");

// Définition des routes pour les messages
router.get("/", messageController.getMessages);
router.post("/", messageController.addMessage);

module.exports = router;
