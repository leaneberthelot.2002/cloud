const express = require('express');
const router = express.Router();
const topicController = require('../controllers/topicController');

// Définition des routes pour les topics et subscriptions
router.post('/create', topicController.createTopic);
router.delete('/delete/:topicName', topicController.deleteTopic);

module.exports = router;