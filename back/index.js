const express = require("express");
const { createServer } = require("node:http");
const bodyParser = require("body-parser");
const messageRoutes = require("./src/routes/messages");
const topicRoutes = require("./src/routes/topics");
const subscriptionRoutes = require("./src/routes/subscriptions");
const { Server } = require("socket.io");
const pubsubService = require("./src/services/pubsubService");

const app = express();
const server = createServer(app);
const io = new Server(server, {
  cors: {
    origin: "*",
  },
});
const port = 3000;

app.use(bodyParser.json());

// Utilisation des routes
app.use("/messages", messageRoutes);
app.use("/topics", topicRoutes);
app.use("/subscriptions", subscriptionRoutes);

let messageArray = [];

const subscription = pubsubService.topic("leane").subscription("leane-sub");
subscription.on("message", (message) => {
  messageArray.push(message.attributes.name);

  io.emit("messageFromServer", message.attributes.name);
});

io.on("connection", (socket) => {
  socket.on("messageFromClient", (message) => {
    pubsubService.topic("leane").publishMessage({
      data: Buffer.from(message),
      attributes: {
        name: message,
      },
    });
  });
});

server.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
});
